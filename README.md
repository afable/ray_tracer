
Erik Afable
Project: Simple Ray Tracer

---------------------------------------
Compile/Run instructions:
---------------------------------------

	Run make from /src. This project was built only for the MAC/OSX platform.

	This program can either be run after making the build in /src or can be called directly from /bin:
		/bin/a.out	--> will run the program

	Features while running the program:
	  'r' --> applies a red light filter to the scene.
	  'g' --> applies a green light filter to the scene.
	  'b' --> applies a blue light filter to the scene.
	  'n' --> removes light filters and shows all colors.

---------------------------------------
Solution Methods:
---------------------------------------

	Ray Tracer:

	To create the simple ray tracer, a ray is shot out from the eye (0, 0, 2, 1) to all directions within a viewing window (0, 0, 1, 1). A point light source is created with a point location, a color, and ambient, diffuse, and specular values. Objects are defined in RTObject.cpp/h for spheres, planes, and triangles. The objects provide ray intersection and surface normal calculation methods and object name and material (Material.h) fields.

	All objects are initialized in main: initScene(). Material properties for each object is also set here and the objects are added to the end of a vector list that is iterated when testing if a ray has intersected with a closest object.

	Shading of object colour (shader.cpp) is done in several steps:

		ambient --> materialAmbient*lightAmbient
		diffuse --> materialDiffuse*cos(theta)*lightDiffuse*distanceAttenuation where theta is the angle of light reflected between light source and surface normal
		specular --> amount of reflected light raised to a phong component (20) multiplied by a distance attenuation factor
		reflection --> incident ray is reflected using surface normal and sent out to grab the colour of the closest object hit with reflection
		refraction --> snell's law was used to calculate refraction
		shadows --> for each point that the eye can see, if there is an object between that point and the light source, place the point in shadow


---------------------------------------
Source Acknowledgement:
---------------------------------------

	-Dr. Amy Gooch's (http://webhome.csc.uvic.ca/~agooch/) skeleton code was used and modified in creating this simple ray tracer
	-Code added to the following files:
		main.cpp:
			(103-133) void initCamera(int w, int h) --> changed camera setup and added light and viewing window setup.
			(135-267) void initScene() --> created several objects made out of spheres and triangles
		Material.h:
			(21-30) Material() --> added specular, reflective, and refractive properties to materials
		raytrace.cpp/h:
			(85-91) void set(point &p, float p0, float p1, float p2, float p3) --> easier to set points
			(93-98) void set(color &c, float r, float g, float b) --> easier to set colors
		RTObject.cpp/h:
			(107-153) implemented plane object
			(159-213) implemented triangle object
		structures.h:
			(49-55) create a light structure
		shader.cpp:
			(24-85) overloaded several vector and colour operators for ease of use (*, +, -, ==, !=, <<)
			(126-408) void shade(…) --> implemented shading for diffuse, specular, reflected, refracted, and shadowed lighting
				as well as distance attenuation and light color filters
		The Makefile was recreated using my design (for simplicity).

	-http://www.flipcode.com/archives/Raytracing_Topics_Techniques-Part_3_Refractions_and_Beers_Law.shtml
		--> covered ray tracing topics that were useful

	-Steven Marschner, Cornell University (included document in /docs/raytri.pdf)
		--> understandable method for implementing a triangle

	-Bram de Greve (included document in /docs/reflect_refract_transmission.pdf)
		--> concepts in reflection and refraction










