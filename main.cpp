
/********************************************************************/
/*         Main program file for raytracer							*/
/*         By: Erik Afable											*/
/********************************************************************/

#include "raytrace.h"
#include "lowlevel.h"
#include <iostream>
#include <string>

#ifndef M_PI
#define M_PI 3.1415926538f
#endif

// Local functions
void initScene();
void initCamera(int w, int h);
void display();
void init(int w, int h);
void keyboard(unsigned char key, int x, int y);

// global variables
point eye;
point viewingWindow;
light light1;	// light source1

// Local data
// parameters defining the camera
point viewpoint;
GLfloat pnear;	// distance from viewpoint to image plane
GLfloat fovx;	// x-angle of view frustum
int width;		// width of window in pixels
int height;		// height of window in pixels


std::vector<RTObject*> sceneObjects;	// GLOBAL VECTOR LIST OF OBJECTS!

const int COLOR_FILTER_NONE = 0;
const int COLOR_FILTER_RED = 1;
const int COLOR_FILTER_GREEN= 2;
const int COLOR_FILTER_BLUE = 3;
GLint nColorFilter = COLOR_FILTER_NONE;

// The main program

// Just sets up window and display callback
int main(int argc, char** argv)
{
	int win;
	glutInit(&argc, argv);
	glutInitWindowSize(WINDOW_W, WINDOW_H);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
	
	win = glutCreateWindow("raytrace");
	
	glutSetWindow(win);
	init(WINDOW_W, WINDOW_H);
	
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	
	// setup cout to 2 decimals
	std::cout.std::ios_base::setf(std::ios_base::fixed);
	std::cout.std::ios_base::precision(2);
	
	glutMainLoop();
	
	return 0;
}

void init(int w, int h)
{
	// OpenGL setup
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);  
	
	// low-level graphics setup
	initCanvas(w,h);
	
	// raytracer setup
	initCamera(w,h);
	initScene();
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	drawScene(::nColorFilter);  // draws the picture in the canvas
	flushCanvas();  // draw the canvas to the OpenGL window
	glFlush();
}

/* Camera placement. 
 NOTE: The placement of the image rectangle is hard-coded 
 to be parallel to the x-y plane, in the plane 
 z=(viewpoint.z = - pnear) (that is, looking down the negative
 z axis, just like OpenGL), with the viewpoint centered with 
 respect to x,y.*/
void initCamera(int w, int h)
{
	/*************************************
	 ******	SET UP CAMERA HERE!!!!! ******
	 *************************************/
	
	viewpoint.x = 0.0;
	viewpoint.y = 0.0;
	viewpoint.z = 2.0;
	viewpoint.w = 1.0;
	pnear = 1.0;
	fovx = M_PI / 6;
	// window dimensions
	width = w;
	height = h;
	
	// set up eye, viewing window, and light source
	eye = viewpoint;
	viewingWindow = eye;
	viewingWindow.z = pnear;
	
	/*************************************
	 ******	SET UP LIGHTS HERE!!!!! ******
	 *************************************/
	
	// set up light1 position and color
	set(light1.p, -2.0f, 1.0f, 2.0f, 1.0f);
	set(light1.amb, 1.0f, 1.0f, 1.0f);
	set(light1.dif, 1.0f, 0.0f, 0.0f);
	set(light1.spec, 0.5f, 1.0f, 0.0f);
}

void initScene()
{
	/*************************************
	 ******	CREATE OBJECTS HERE!!!!! *****
	 *************************************/
	
	// bear skin
	RTObject *oHead, *oLeftEar, *oRightEar, *oBeard;;
	// bear features
	RTObject *oLeftEarFace, *oRightEarFace, *oLeftEye, *oRightEye, *oLeftPupil, *oRightPupil, *oNose, *tMouth;
	// environment triangles
	RTObject *groundLeft, *groundCenter, *groundRight;
	// am i doing it right triangle
	RTObject *tri1, *tri2, *tri3;
	// refractive crystal ball
	RTObject *oCrystalBall;
	
	// create a new rto
	oHead = new RTSphere(-0.2f, 0.0f, 0.0f, 0.3f, "Bear Head");	// RTSphere(x, y, z, radius)
	oLeftEar = new RTSphere(-0.42f, 0.22f, 0.0f, 0.1f, "Bear Left Ear");
	oRightEar = new RTSphere(0.02f, 0.22f, 0.0f, 0.1f, " Bear Right Ear");
	oBeard = new RTSphere(-0.2f, -0.05f, 0.2f, 0.15f, "Bear Beard");
	oLeftEarFace = new RTSphere(-0.42f, 0.22f, 0.1f, 0.05f, "Bear Left Ear patch");
	oRightEarFace = new RTSphere(0.02f, 0.22f, 0.1f, 0.05f, "bear right ear patch");
	oLeftEye = new RTSphere(-0.23f, 0.14f, 0.27f, 0.03f, "bear left eye white");
	oRightEye = new RTSphere(-0.17f, 0.14f, 0.27f, 0.03f, "bear right eye white");
	oLeftPupil = new RTSphere(-0.215f, 0.143f, 0.30f, 0.02, "bear left eye pupil");
	oRightPupil = new RTSphere(-0.155f, 0.143f, 0.30f, 0.02, "bear right eye pupil");
	oNose = new RTSphere(-0.2f, -0.02f, 0.4f, 0.1f, "bear nose");
	
	// bear-chan's mouth
	point p0 = {-0.218f, -0.13f, 0.5f, 1.0f};
	point p1 = {-0.22f, -0.14f, 0.5f, 1.0f};
	point p2 = {-0.131f, -0.127f, 0.5f, 1.0f};
	tMouth = new RTTriangle(p0, p1, p2, "bear mouth");
	
	// create ground left
	set(p0, -1.0f, 0.0f, 0.0f, 1.0f);
	set(p1, -1.0f, -1.0f, 0.0f, 1.0f);
	set(p2, 0.0f, 0.2f, 0.0f, 1.0f);
	groundLeft = new RTTriangle(p0, p1, p2, "left ground triangle");
	
	// create ground center
	set(p0, 0.0f, 0.2f, 0.0f, 1.0f);
	set(p1, -1.0f, -1.0f, 0.0f, 1.0f);
	set(p2, 1.0f, -1.0f, 0.0f, 1.0f);
	groundCenter = new RTTriangle(p0, p1, p2, "middle ground triangle");
	
	// create ground right
	set(p0, 0.0f, 0.2f, 0.0f, 1.0f);
	set(p1, 1.0f, -1.0f, 0.0f, 1.0f);
	set(p2, 1.0f, -0.5f, 0.0f, 1.0f);
	groundRight = new RTTriangle(p0, p1, p2, "right ground triangle");
	
	// create "am i doing it right?" triangle
	set(p0, 0.40f, 0.20f, -1.0f, 1.0f);
	set(p1, 0.60f, 0.20f, -1.0f, 1.0f);
	set(p2, 0.50f, 0.40f, -1.0f, 1.0f);
	tri1 = new RTTriangle(p0, p1, p2, "bot left triangle");
	
	set(p0, 0.60f, 0.20f, -1.0f, 1.0f);
	set(p1, 0.80f, 0.20f, -1.0f, 1.0f);
	set(p2, 0.70f, 0.40f, -1.0f, 1.0f);
	tri2 = new RTTriangle(p0, p1, p2, "bot right triangle");
	
	set(p0, 0.40f, 0.40f, -1.0f, 1.0f);
	set(p1, 0.60f, 0.40f, -1.0f, 1.0f);
	set(p2, 0.50f, 0.60f, -1.0f, 1.0f);
	tri3 = new RTTriangle(p0, p1, p2, "top left triangle");
	
	// refractive crystal ball
	oCrystalBall = new RTSphere(0.3f, 0.2f, -0.5f, 0.1f, "CrystalBall");
	
	
	/*************************************
	 ******	SET MATERIAL PROPERTIES  *****
	 *************************************/
	
	// material properties
	Material mBearSkin, mBearFace, mBearBeard, mBearPupil, mBearNose, mBearMouth, mGround;
	Material mTriangles;
	Material mCrystalBall;
	
	// set new rto material properties
	mBearSkin.set(0.99f, 0.5f, 0.05f, 0.0f, 0.7f, 1.0f, 0.4f, 0.0f);	// 	set(r, g, b, ambientRef, diffusePer, specPer, reflPer, refrInd)
	mBearFace.set(1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f);
	mBearBeard.set(0.99f, 0.5f, 0.05f, 0.3f, 0.7f, 1.0f, 0.4f, 0.0f);	// 	set(r, g, b, ambientRef, diffusePer, specPer, reflPer, refrInd)
	mBearPupil.set(0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 0.0f);
	mBearNose.set(0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f);
	mBearMouth.set(1.0f, 0.1, 0.1f, 0.44f, 0.6f, 0.0f, 0.0f, 0.0f);
	
	// other materials
	mGround.set(0.1f, 0.6f, 0.15f, 0.3f, 0.6f, 0.0f, 0.0f, 0.0f);
	
	// triangles
	mTriangles.set(1.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 0.0f);
	
	// crystal ball
	mCrystalBall.set(0.2f, 0.2f, 0.2f, 0.2f, 0.2f, 0.0f, 0.0f, 1.33f);	// refractive index of water ~1.33
	
	
	oHead->setMaterial(mBearSkin);
	oLeftEar->setMaterial(mBearSkin);
	oRightEar->setMaterial(mBearSkin);
	oBeard->setMaterial(mBearBeard);
	oLeftEarFace->setMaterial(mBearFace);
	oRightEarFace->setMaterial(mBearFace);
	oLeftEye->setMaterial(mBearFace);
	oRightEye->setMaterial(mBearFace);
	oLeftPupil->setMaterial(mBearPupil);
	oRightPupil->setMaterial(mBearPupil);
	oNose->setMaterial(mBearNose);
	tMouth->setMaterial(mBearMouth);
	
	groundLeft->setMaterial(mGround);
	groundCenter->setMaterial(mGround);
	groundRight->setMaterial(mGround);
	
	tri1->setMaterial(mTriangles);
	tri2->setMaterial(mTriangles);
	tri3->setMaterial(mTriangles);
	
	oCrystalBall->setMaterial(mCrystalBall);
	
	
	/*************************************
	 ******	 PUSH OBJECTS TO VECTOR  *****
	 *************************************/
	
	// push to back of std vector sceneObjects (global variable)
	sceneObjects.push_back(oHead);
	sceneObjects.push_back(oLeftEar);
	sceneObjects.push_back(oRightEar);
	sceneObjects.push_back(oBeard);
	sceneObjects.push_back(oLeftEarFace);
	sceneObjects.push_back(oRightEarFace);
	sceneObjects.push_back(oLeftEye);
	sceneObjects.push_back(oRightEye);
	sceneObjects.push_back(oLeftPupil);
	sceneObjects.push_back(oRightPupil);
	sceneObjects.push_back(oNose);
	sceneObjects.push_back(tMouth);
	
	sceneObjects.push_back(groundLeft);
	sceneObjects.push_back(groundCenter);
	sceneObjects.push_back(groundRight);
	
	sceneObjects.push_back(tri1);
	sceneObjects.push_back(tri2);
	sceneObjects.push_back(tri3);
	
	sceneObjects.push_back(oCrystalBall);
}

void keyboard (unsigned char key, int x, int y)
{
	switch ( key )
	{
		case 'q':
			exit(EXIT_SUCCESS);
			break;
		case 'r':
			::nColorFilter = COLOR_FILTER_RED;
			break;
		case 'g':
			::nColorFilter = COLOR_FILTER_GREEN;
			break;
		case 'b':
			::nColorFilter = COLOR_FILTER_BLUE;
			break;
			
		case 'n':
		default:
			::nColorFilter = COLOR_FILTER_NONE;
			break;
	}
	glutPostRedisplay();
}
















