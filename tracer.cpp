
/******************************************************************/
/*         Functions to trace rays								  */
/******************************************************************/

#include "raytrace.h"

extern std::vector<RTObject*> sceneObjects;	// from main.cpp


// Point on ray r parameterized by t is returned in p
void findPointOnRay(const ray &r, double t, point &p)
{
	p.x = (float)(r.start->x + t * r.end->x);
	p.y = (float)(r.start->y + t * r.end->y);
	p.z = (float)(r.start->z + t * r.end->z);
	p.w = 1.0f;
}

// If something is hit, returns the finite intersection point in p, 
// and the object hit as return value. If no hit, returns an
// infinite point (p->w = 0.0) and NULL value
RTObject* trace(const ray &r, point &p, double bestT /* = 99999 */)
{
	RTObject *closest = NULL;
	
	// Check all objects in scene for intersection
	std::vector<RTObject*>::iterator it;	// just helps iterate through the vector list, iterator of type RTObject *
	
	for ( it = sceneObjects.begin(); it < sceneObjects.end(); ++it )
	{
		double t = (*it)->intersection(r); // t <-- positive value if something hits, 0 otherwise
		
		if ( t > INTERSECTION_TOLERANCE && t < bestT )
		{
			closest = *it;
			bestT = t;	// lowest value of t means closest point hit (t is the dot product of two vectors testing for intersection)
		}
	}
	
	if ( closest == NULL )
	{
		p.w = 0.0;	// nothing hit, set to infinite point
		return NULL;
	}
	else
	{
		findPointOnRay(r, bestT, p);	// point where hit set here with w=1.0f
		return closest;		// return closest RTObject that was hit
	}
}
