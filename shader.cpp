
/******************************************************************/
/*         Lighting functions									  */
/******************************************************************/

#include "raytrace.h"
#include "Material.h"
#include <iostream>
#include <cmath>
#include <string>

const int COLOR_FILTER_NONE = 0;
const int COLOR_FILTER_RED = 1;
const int COLOR_FILTER_GREEN = 2;
const int COLOR_FILTER_BLUE = 3;

const int MAX_DISTANCE_ATTENUATION = 99999;

extern point eye;
extern light light1; // light source1

// These first two functions aren't used currently, they're just
// left in for your convenience
vector operator* (const point &p, float scalar)
{
	vector v;
	v.x = p.x * scalar;
	v.y = p.y * scalar;
	v.z = p.z * scalar;
	v.w = 1.0;
	return v;
}

vector operator* (float scalar, const point &p)
{
	return p*scalar;
}

vector operator+ (const vector &v1, const vector &v2)
{
	vector v;
	v.x = v1.x + v2.x;
	v.y = v1.y + v2.y;
	v.z = v1.z + v2.z;
	v.w = 1.0;
	return v;
}

vector operator- (const vector &v1, const vector &v2)
{
	vector v;
	v.x = v1.x - v2.x;
	v.y = v1.y - v2.y;
	v.z = v1.z - v2.z;
	v.w = 0.0f;
	return v;
}

bool operator== (const vector &v1, const vector &v2)
{
	return (v1.x == v2.x &&
			v1.y == v2.y &&
			v1.z == v2.z &&
			v1.w == v2.w);
}

bool operator!= (const vector &v1, const vector &v2)
{
	return !(v1 == v2);
}

std::ostream & operator<< (std::ostream &os, const vector &v)
{
	os << "(" << v.x << "," << v.y << "," << v.z << "," << v.w << ")";
	
	return os;
}

std::ostream & operator<< (std::ostream &os, const color &c)
{
	os << "(" << c.r << "," << c.g << "," << c.b << ")";
	
	return os;
}

// Inlines
inline float length(const vector &v)
{
	return sqrtf(v.x * v.x + v.y * v.y + v.z * v.z);
}

// Note: normalizes v
void normalize(vector *v)
{
	float lngth = length(*v);
	v->x /= lngth;
	v->y /= lngth;
	v->z /= lngth;
}

// return a fracted vector direction
vector refract(const vector &normal, const vector &incident, GLfloat n1, GLfloat n2)
{
	const GLfloat n = n1 / n2;
	const GLfloat cosI = dotproduct(normal, incident);
	const GLfloat sinT2 = n * n * (1.0f - cosI * cosI);
	
	if ( sinT2 > 1.0f && sinT2 < 0.0f )
	{
		std::cerr << "invalid refraction!" << std::endl;
		vector invalid;
		return invalid;
	}
	
	return n * incident - (n + sqrtf(1.0f - sinT2))*normal;
}




// LIGHTING CALCULATIONS

// Color of point p on object rto returned in c
// p: point where hit rto object, rto: hit object, r: ray with startpoint worldPix and vector direction, color: will be based on material properties
// SHADE ONLY CALLED IF RAY FROM CAMERA HIT AN RTO OBJECT!
void shade(point &p, RTObject *rto, const ray &r, color* c, GLint nColorFilter, int depth /* = 2 */)	// depth used for reflections (2 bounces)
{
	// Get n and material from rto
	vector n = rto->normalAt(p, *(r.start));	// gimme the normal vector from the point this object was hit yo!
	Material *m = rto->getMaterialPointer();	// m just needs to be set by void setMaterial(const Material &mat) { m = mat; }
	
	// reset ray color
	c->r = 0.0f;
	c->g = 0.0f;
	c->b = 0.0f;
	
	/* 
	 material default:
	 c.r = 1.0;
	 c.g = 1.0;
	 c.b = 1.0;
	 amb = 1.0;
	 dif = 1.0;
	 
	 Camera placement:
		viewpoint.x = 0.0;
		viewpoint.y = 0.0;
		viewpoint.z = 2.0;
		viewpoint.w = 1.0;
		pnear = 1.0;
	 */
	
	
	/************************************
	 * Ambient part (ambient color = c.light * c.material)
	 ************************************/
	color cAmb;
	cAmb.r = m->amb * m->c.r;
	cAmb.g = m->amb * m->c.g;
	cAmb.b = m->amb * m->c.b;
	
	
	/************************************
	 * Diffuse part
	 ************************************/
	
	// can the hit point on object p see light source? if so, at what angle? cos(theta) between p to camera and p to light should give us amount
	// also check normal ... should have something to do with seeing the light?
	vector vPToEye = eye - p;
	vector vPToLight = light1.p - p;
	
	// normalize vectors to make them unit vectors
	normalize(&vPToEye);
	normalize(&vPToLight);
	
	// cosTheta gives us how much light gets reflected on the first bounce of light source and surface normal where rto hit
	GLfloat cosTheta = dotproduct(n, vPToLight);
	
	// cDif will be the diffuse color part
	color cDif = {0.0f, 0.0f, 0.0f};
	
	// if cosTheta is negative, then the hit point p cannot see the light!
	if ( cosTheta > 0.0f )
	{
		// apply the diffuse color values
		cDif.r = m->dif * m->c.r * cosTheta;
		cDif.g = m->dif * m->c.g * cosTheta;
		cDif.b = m->dif * m->c.b * cosTheta;
	}
	
	
	/************************************
	 * Specular part
	 ************************************/
	
	// v is the view direction
	vector v = *(r.end);
	
	// l is the vector from light source to point
	vector l = p - light1.p;
	
	// r is l reflected in the surface
	vector ref = l - 2.0f*dotproduct(l, n)*n;
	normalize(&ref);
	
	float dot = dotproduct(v, ref);
	color cSpec = {0.0f, 0.0f, 0.0f};
	
	if ( dot < 0.0f )	// correcting for specular that appears from back surfaces
	{
		float spec = powf(dot, 20.0f);
		
		cSpec.r += spec * m->spec;
		cSpec.g += spec * m->spec;
		cSpec.b += spec * m->spec;
	}
	
	/************************************
	 * Calculate reflections
	 ************************************/
	
	// calculate reflected ray
	ray reflected;
	
	// move start point back along direction a little to account for rays reflected on their own objects
	point refStart = p;
	refStart = p + (-INTERSECTION_TOLERANCE * (*r.end));
	reflected.start = &refStart;
	
	vector refl = *r.end - 2.0f * ( dotproduct(*r.end, n) ) * n;
	reflected.end = &refl;
	
	point reflectedHitPoint;
	
	// test if there is an object in the reflected direction
	RTObject *rtoReflected = trace(reflected, reflectedHitPoint);	// reflectedHitPoint.w = 1.0 if hit; rto = closest hit object, null if no hit
	
	// declare reflected color properties
	color cRef = {0.0f, 0.0f, 0.0f};
	
	// Do the reflection calculations only if something was hit!
	if ( reflectedHitPoint.w != 0.0 && depth-- > 0 && m->refl )
	{
		// reflected hit point, rto hit object, reflected ray, color, num bounces
		shade(reflectedHitPoint, rtoReflected, reflected, &cRef, nColorFilter, depth);
	}
	
	/************************************
	 * Calculate refractions
	 ************************************/
	
	color cRefr = { 0.0f, 0.0f, 0.0f };
	GLfloat air = 1.0003f;
	
	// if hit object material has a refractive index, let's do refraction!
	if ( m->refr )
	{
		// get refracted inside ray direction
		vector refractInside = refract(n, *r.end, air, m->refr);
		
		// test if vector is zero vector, if so we quit
		vector zero = {0.0f, 0.0f, 0.0f, 0.0f};
		
		if ( refractInside == zero )
			std::cout << "ZERO!" << std::endl;
		if ( refractInside != zero )
		{
			// move refracted ray start point along vector direction to account for objects refracting from same surface
			point refrStart = p + INTERSECTION_TOLERANCE*(*r.end);
			
			// setup refract inside ray
			ray refractInsideRay;
			refractInsideRay.start = &refrStart;
			refractInsideRay.end = &refractInside;
			
			// assign point where refracted inside ray hits other side of object (that goes to air) here
			point refractedHitInside;
			
			// trace for other side of object
			RTObject *rtoRefracted = trace(refractInsideRay, refractedHitInside);
			
			
			
			// now refrace outside of the object switching the n values (negative normal since going out back face)
			if ( rtoRefracted )
			{
				vector nOut = rtoRefracted->normalAt(refractedHitInside, *refractInsideRay.start);
				Material *mOut = rtoRefracted->getMaterialPointer();
				
				// if other side has refractive index (it definitely should), let's refract outside of this object
				if ( mOut->refr )
				{
					// get refract to outside ray direction and test if it is valid (not 0.0f, 0.0f, 0.0f)
					vector refractOutside = refract(nOut, *refractInsideRay.end, mOut->refr, air);	// notice we're going from material to air now
					
					if ( refractOutside == zero )
						std::cout << "ZERO!" << std::endl;
					
					if ( refractOutside != zero )
					{
						// move refracted ray start point along vector direction to account for objects refracting from same surface
						point refrOutStart = refractedHitInside + INTERSECTION_TOLERANCE*(*refractInsideRay.end);
						
						// setup refract outside ray
						ray refractOutsideRay;
						refractOutsideRay.start = &refrOutStart;
						refractOutsideRay.end = &refractOutside;
						
						// assign point where refracted outside ray hits a new object here
						point refractOutHit;
						
						// trace for an object hit with the outside refracted ray
						RTObject *rtoRefractOut = trace(refractOutsideRay, refractOutHit);
						
						// if we hit something, let's get the hit object's colors and add it to our refractive color!
						if ( rtoRefractOut )
						{	
							// p: point where hit rto object, rto: hit object, r: ray with startpoint worldPix and vector direction, color: will be based on material properties
							shade(refractOutHit, rtoRefractOut, refractOutsideRay, &cRefr, nColorFilter);
						}
					}
				}
			}
		}
	}
	
	
	/************************************
	 * Calculate shadows
	 ************************************/
	
	// create a new ray from point where hit to light
	ray shadowRay;
	shadowRay.start = &p;
	vector shadowDir = light1.p - p;
	shadowRay.end = &shadowDir;
	
	point shadowHitPoint;
	
	// test if there are any objects inbetween p and light
	RTObject *shadower = trace(shadowRay, shadowHitPoint);
	
	// declare shadow boolean
	bool pixShadowed = false;
	
	// if there's an object between our start point and light, then shade this pixel! TEST ALL LIGHTS!
	if ( shadower )
	{
		pixShadowed = true;
	}
	
	

	/************************************
	 * Calculate distance attenuation factor (maximum distance is 99999)
	 ************************************/
	GLfloat da = (::MAX_DISTANCE_ATTENUATION - length(light1.p - p))/::MAX_DISTANCE_ATTENUATION;
	
	
	/************************************
	 * Apply final color formula!
	 ************************************/
	// color += ambient + (distance attenuation)*(diffuse + specular) + reflective
	c->r += light1.amb.r*cAmb.r + da*(light1.dif.r*cDif.r + light1.spec.r*cSpec.r) + cRef.r*m->refl + cRefr.r*m->c.r;
	c->g += light1.amb.g*cAmb.g + da*(light1.dif.g*cDif.g + light1.spec.g*cSpec.g) + cRef.g*m->refl + cRefr.g*m->c.g;
	c->b += light1.amb.b*cAmb.b + da*(light1.dif.b*cDif.b + light1.spec.b*cSpec.b) + cRef.b*m->refl + cRefr.b*m->c.b;
	
	/************************************
	 * Apply color filter
	 ************************************/
	switch ( nColorFilter )
	{
		case COLOR_FILTER_RED:
			c->g = 0.0f;
			c->b = 0.0f;
			break;
		case COLOR_FILTER_GREEN:
			c->r = 0.0f;
			c->b = 0.0f;
			break;
		case COLOR_FILTER_BLUE:
			c->r = 0.0f;
			c->g = 0.0f;
			break;
			
		case COLOR_FILTER_NONE:
		default:
			break;
	}
	
	/************************************
	 * Apply shadow filter
	 ************************************/
	if ( pixShadowed )
	{
		c->r *= 0.8f;	// 80% to apply a soft shadow
		c->g *= 0.8f;
		c->b *= 0.8f;
	}
	
	/************************************
	 * Clamp extreme color values to 1.0
	 ************************************/
	if (c->r > 1.0) c->r = 1.0;
	if (c->g > 1.0) c->g = 1.0;
	if (c->b > 1.0) c->b = 1.0;
	
}
